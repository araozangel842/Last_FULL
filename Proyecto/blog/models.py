from django.db import models

from django.contrib.auth.models import User 

# Create your models here.

class Category(models.Model):
     category_name = models.CharField(max_length=50)
     created_at = models.DateTimeField(auto_now_add=True)
     updated_at = models.DateTimeField(auto_now_add= True)

     class Meta:
          verbose_name = 'Category'
          verbose_name_plural = 'Categories'
     
     def __str__(self):
          return self.category_name

class Post (models.Model):
     title = models.CharField(max_length=50)
     content = models.TextField(max_length=50)
     image = models.ImageField(upload_to= 'blog', null=True, blank=True)
     author = models.ForeignKey(User, on_delete=models.CASCADE)
     categories = models.ManyToManyField(Category)
     created_at = models.DateTimeField(auto_now_add=True)
     updated_at = models.DateTimeField(auto_now_add= True)

     class Meta:
          verbose_name = 'Post'
          verbose_name_plural = 'Postes'
     
     def __str__(self):
          return self.title 