from django.shortcuts import render
from blog.models import Post, Category

# Create your views here.

def blog(request):
     post = Post.objects.all()

     return render(request, 'blog/blog.html',{"post":post})

def category(request, category_id):
     category = Category.objects.get(id=category_id)
     post = Post.objects.filter(categories=category)

     return render(request,'blog/category.html',{"category":category, "post": post})
