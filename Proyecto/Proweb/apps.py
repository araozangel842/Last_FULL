from django.apps import AppConfig


class ProwebConfig(AppConfig):
    name = 'Proweb'
