class Shopping:
     def __init__(self, request):
          self.request = request
          self.session = request.session
          shop = self.session.get("shop")
          if not shop:
               shop = self.session["shop"]={}
          else:
               self.shop = shop
     
     # Función encargada de agregar al carrito
     def add_shop(self, product):
          if(str(product.id) not in self.shop.keys()):
               self.shop[product.id] ={
                    "product_id": product.id,
                    "nombre": str(product.name),
                    "precio" : product.price,
                    "cantidad" : 1,
                    "image" : product.image.url
               }
          else:
               # Bucle para sumar productos al carrito
               for key , value in self.shop.items():
                    if key == str(product.id):
                         value["cantidad"]=value["cantidad"]+1
                         break
          self.add_shop_full()

     # Guarda cambios realizado en el carrito
     def add_shop_full(self):
          self.session["shop"]= self.shop
          self.session.modified = True

     # Eliminar un producto
     def delete(self, product):
          product.id = str(product.id)
          if product.id in self.shop:
               del self.shop[product.id]
               self.add_shop_full()

     # Restar un producto del carrito
     def delete_product(self, product):
          for key, value in self.shop.items():
               if key == str(product.id):
                    value["cantidad"]=value["cantidad"]-1
                    if value["cantidad"]<1:
                         self.delete(product)
                    break
          self.add_shop_full()

     # Limpiar todo el carrito
     def clean_shop(self):
          self.session["shop"]={}
          self.session.modified = True
