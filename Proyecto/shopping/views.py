from django.shortcuts import render

from .shopping import Shopping

from Store.models import Product

from django.shortcuts import redirect

# Create your views here.
def add_product(request, product_id):

     shop = Shopping(request)

     product = Product.objects.get(id=product_id)

     shop.add_shop(product= product)

     return redirect("Store")


def delete_product(request, product_id):

     shop = Shopping(request)

     product = Product.objects.get(id=product_id)

     shop.delete(product= product)

     return redirect("Store")



def del_product(request, product_id):

     shop = Shopping(request)

     product = Product.objects.get(id=product_id)

     shop.delete_product(product= product)

     return redirect("Store")


def clean_all(request, product_id):

     shop = Shopping(request)

     shop.clean_shop()

     return redirect("Store")