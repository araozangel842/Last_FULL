from django.urls import path

from . import views

# Importar desde la app Services la url para las imagenes
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.service, name='Service'),
  
]

# Se traen las URLS del settings.py Proyecto
urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)