from django.db import models

# Create your models here.
class Service(models.Model):
     title = models.CharField(max_length=50, verbose_name='Titles')
     content = models.TextField(max_length=50, verbose_name='Content')
     image = models.ImageField(verbose_name= 'Image', upload_to= 'Services')
     created_at = models.DateTimeField(auto_now_add=True)
     updated_at = models.DateTimeField(auto_now_add= True)

     class Meta:
          verbose_name = 'Service'
          verbose_name_plural = 'Services'
     
     def __str__(self):
          return self.title
