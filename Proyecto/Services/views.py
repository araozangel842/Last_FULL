from django.shortcuts import render

from .models import Service

# Create your views here.
def service(request):
     services = Service.objects.all()

     return render(request, 'Services/service.html',{
          "services": services
     })