from django.contrib import admin
from .models import CategoryProduct, Product

# Register your models here.
class CategoryProductAdmin(admin.ModelAdmin):
     readonly_fields =('created_at', 'updated_at')

class ProductAdmin(admin.ModelAdmin):
     readonly_fields =('created_at', 'updated_at')

admin.site.register(CategoryProduct, CategoryProductAdmin)
admin.site.register(Product, ProductAdmin)