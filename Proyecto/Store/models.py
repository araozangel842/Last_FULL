from django.db import models

# Create your models here.
class CategoryProduct(models.Model):
     name = models.CharField(max_length=50)
     created_at = models.DateTimeField(auto_now_add= True)
     updated_at = models.DateTimeField(auto_now_add=True)

     class Meta:
          verbose_name = "categoryproduct"
          verbose_name_plural = "categoriesProd"
     def __str__(self):
          return self.name

class Product(models.Model):
     name = models.CharField(max_length=50)
     categories = models.ForeignKey(CategoryProduct, on_delete=models.CASCADE)
     image = models.ImageField(upload_to= 'Store', blank=True, null=True)
     price = models.FloatField()
     stoke = models.BooleanField(default=True)
     created_at = models.DateTimeField(auto_now_add= True)
     updated_at = models.DateTimeField(auto_now_add= True)

     class Meta:
          verbose_name = 'Product'
          verbose_name_plural = 'Products'

