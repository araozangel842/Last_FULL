from django import forms

class ContactForm(forms.Form):

     form_name = forms.CharField(max_length=20, required=True, label= "Name")

     email = forms.CharField(max_length= 20, required=True, label= "Email")

     content = forms.CharField(required=True, label="Content", widget=forms.Textarea)