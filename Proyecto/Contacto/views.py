from django.shortcuts import render

from .forms import ContactForm

from django.core.mail import EmailMessage

# Create your views here.
def contact(request):
     # form_contact = ContactForm()

     if request.method == 'POST':
          form_contact = ContactForm(data = request.POST)

          if form_contact.is_valid():
               name = request.POST.get('name')
               email = request.POST.get('email')
               content = request.POST.get('content')

               email = EmailMessage("Mensaje desde mi app en Django", "User with name {} with address {} write this message:\n\n {}".format(name,email,content),
               "",["angelito.cuba1984@gmail.com"], reply_to=[email])
               try:
                    email.send()
                    return render(request, 'Contacto/gracias.html',)   
               except:
                    return render(request,'Contacto/404.html')  
                               
     else:
          form_contact = ContactForm()           
     return render(request, 'contacto/contact.html', {'form_contact': form_contact})